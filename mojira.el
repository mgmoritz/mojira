(eval-when-compile (require 'cl))
(require 'org)
(require 'json)
(require 'mopass)
(require 'motime)
(require 'request)
(require 'org-colview)

(defun mojira--parse-utf-8 ()
  (json-read-from-string
   (decode-coding-string (buffer-string) 'utf-8)))

(defun mojira--get-issues (sprint)
  (append (cdr (assoc 'issues sprint)) nil))

(defun mojira--get-sprint-name (issues)
  (cdr (assoc 'name (mojira--get-issue-field-property (car issues) "sprint"))))

(defun mojira--get-board-id (issues)
  (cdr (assoc 'originBoardId (mojira--get-issue-field-property (car issues) "sprint"))))

(defun mojira--get-sprint-id (issues)
  (cdr (assoc 'id (mojira--get-issue-field-property (car issues) "sprint"))))

(defun mojira--get-original-estimate-seconds (issue)
  (let* ((timetracking (mojira--get-issue-field-property issue "timetracking"))
         (originalEstimateSeconds (if (and timetracking (assoc 'originalEstimateSeconds timetracking))
                                      (cdr (assoc 'originalEstimateSeconds timetracking))
                                    0)))
    (motime-format-seconds originalEstimateSeconds)))

(defun mojira--get-timespent-seconds (issue)
  (let* ((timetracking (mojira--get-issue-field-property issue "timetracking"))
         (timeSpentSeconds (if (and timetracking (assoc 'timeSpentSeconds timetracking))
                               (cdr (assoc 'timeSpentSeconds timetracking))
                             0)))
    (motime-format-seconds timeSpentSeconds)))

(defun mojira--get-time-remaining-seconds (issue)
  (let* ((timetracking (mojira--get-issue-field-property issue "timetracking"))
         (originalEstimateSeconds (if (and timetracking (assoc 'originalEstimateSeconds timetracking))
                                      (cdr (assoc 'originalEstimateSeconds timetracking))
                                    0))
         (timeSpentSeconds (if (and timetracking (assoc 'timeSpentSeconds timetracking))
                               (cdr (assoc 'timeSpentSeconds timetracking))
                             0)))
    (motime-format-seconds (- originalEstimateSeconds
                              timeSpentSeconds))))

(defun mojira--get-assignee (issue)
  (let ((assignee (mojira--get-issue-field-property issue "assignee")))
    (if (and assignee (assoc 'displayName assignee))
        (cdr (assoc 'displayName assignee)))))

(defun mojira--count-sprint-issues (sprint)
  (length (mojira--get-issues sprint)))

(defun mojira--get-issue-field-property (issue property)
  (cdr (assoc (intern property) (assoc 'fields issue))))

(defun mojira--get-issue-status (issue)
  (cdr (assoc 'name (mojira--get-issue-field-property issue "status"))))

(defun mojira--get-issue-status-category (issue)
  (cdr (assoc 'name (assoc 'statusCategory (mojira--get-issue-field-property issue "status")))))

(defun mojira--is-subtask (issue)
  (not (eq (cdr (assoc 'subtask
                       (mojira--get-issue-field-property issue "issuetype")))
           json-false)))

(defun mojira--get-base64-token ()
  (base64-encode-string
   (format "%s:%s"
           (mopass--get-prop-by-name "bitbucket" 'email)
           (mopass--get-prop-by-name "bitbucket" 'token))))

(defvar mojira--api-v2
  "https://ptmdev.atlassian.net/rest/api/2"
  "Jira API V2")

(defvar mojira--api-v3
  "https://ptmdev.atlassian.net/rest/api/3"
  "Jira API V3")

(defcustom mojira--user
  "Marcos Moritz"
  "Jira display name"
  :type 'string
  :group 'mojira)

(defun mojira--add-worklog (issue time)
  "Get the issue logged time in seconds"
  (request
    (format "%s/issue/%s/worklog" mojira--api-v2 issue)
    :parser 'json-read
    :data (json-encode `(("timeSpentSeconds" . ,time)
                         ("comment" . "[mojira]")))
    :headers `(("Content-Type" . "application/json")
               ("Authorization" . ,(concat "Basic " (mojira--get-base64-token))))
    :success
    (cl-function
     (lambda (&key data &allow-other-keys)
       (when data
         (message "%S" data))))))

(defun mojira--filter-worklogs (worklogs filters)
  (let ((current-filter (car filters)))
    (if current-filter
        (mojira--filter-worklogs
         (cl-remove-if-not
          (lambda (wl)
            (string-equal (cdr current-filter)
                          (alist-get (car current-filter)
                                     (assoc 'author wl))))
          worklogs)
         (cdr filters))
      worklogs)))

(defun mojira--get-worklog-times (data &optional filters)
  (mapcar (lambda (wl)
            (alist-get 'timeSpentSeconds wl))
          (mojira--filter-worklogs
           (alist-get 'worklogs data)
           filters)))

(defun mojira--get-worklogs (issue callback &optional filters)
  "Get the issue logged time in seconds"
  (lexical-let ((filters filters)
                (callback callback))
    (request
      (format "%s/issue/%s/worklog" mojira--api-v2 issue)
      :parser 'json-read
      :headers `(("Content-Type" . "application/json")
                 ("Authorization" . ,(concat "Basic " (mojira--get-base64-token))))
      :success
      (cl-function
       (lambda (&key data &allow-other-keys)
         (when data
           (let*
               ((worklog
                 (cl-reduce
                  '+
                  (mojira--get-worklog-times data filters))))
             (funcall callback worklog))))))))

(defun mojira--get-issue (issue callback)
  "Get the issue"
  (lexical-let ((callback callback))
    (request
      (format "%s/issue/%s" mojira--api-v2 issue)
      :parser 'json-read
      :headers `(("Content-Type" . "application/json")
                 ("Authorization" . ,(concat "Basic " (mojira--get-base64-token))))
      :success
      (cl-function
       (lambda (&key data &allow-other-keys)
         (when data
           (funcall callback data)))))))

(defun mojira--create-issue (data callback)
  "Create issue"
  (lexical-let ((callback callback))
    (request
      (format "%s/issue" mojira--api-v3)
      :type "POST"
      :data (json-encode data)
      :parser 'json-read
      :headers `(("Content-Type" . "application/json")
                 ("Authorization" . ,(concat "Basic " (mojira--get-base64-token))))
      :error
      (cl-function
       (lambda (&key data &allow-other-keys)
         (when data
           (message data))))
      :success
      (cl-function
       (lambda (&key data &allow-other-keys)
         (when data
           (funcall callback data)))))))

(defun mojira--log-work (taskid clocksum)
  (lexical-let ((taskid taskid)
                (orgseconds (* 60 (org-duration-to-minutes clocksum))))
    (if (> orgseconds 0)
        (mojira--get-worklogs
         taskid
         (lambda (data)
           (let ((diff (- orgseconds data)))
             (if (> diff 0)
                 (mojira--add-worklog taskid diff))))
         `((displayName . ,mojira--user))))))

(defun mojira--set-task-id ()
  (let ((taskid (mojira--get-issue-name)))
    (if taskid
        (org-set-property "JIRA_TASK_ID" (mojira--get-issue-name)))))

(defun mojira--get-issue-name ()
  (let ((str (thing-at-point 'line)))
    (when (string-match "[A-Z]\\{2,4\\}-[0-9]\\{1,5\\}" str)
      (match-string 0 str))))

(defun mojira-browse-issue ()
  "Browse jira issue at point."
  (interactive)
  (let (pos1 pos2 issue)
    (if (use-region-p)
        (progn
          (setq pos1 (region-beginning)
                pos2 (region-end))
          (setq issue (buffer-substring-no-properties pos1 pos2)))
      (setq issue (mojira--get-issue-name)))
    (browse-url (format "https://ptmdev.atlassian.net/browse/%s" issue))))

(defun mojira-get-logged-for-task-at-point ()
  "Return the jira logged time"
  (interactive)
  (save-excursion
    (org-back-to-heading t)
    (org-columns)
    (org-columns-quit)
    (let* ((properties (org-entry-properties))
           (display-name (or (cdr (assoc "JIRA_USER" properties))
                             mojira--user))
           (taskid (or
                    (cdr (assoc "JIRA_TASK_ID" properties))
                    (progn (mojira--set-task-id)
                           (cdr (assoc "JIRA_TASK_ID" (org-entry-properties)))))))
      (cond
       (taskid (mojira--get-worklogs
                  taskid
                  (lambda (data)
                    (message "Logged time:\t%s" (org-duration-from-minutes (/ (float data) 60))))
                  `((displayName . ,display-name))))
       ((> (org-current-level) 1)
        (progn
          (org-up-element)
          (mojira-get-logged-for-task-at-point)))
       (t (message "No task at point"))))))

(defun mojira-log-work-for-task-at-point ()
  (interactive)
  (org-back-to-heading t)
  (org-columns)
  (org-columns-quit)
  (let* ((properties (org-entry-properties))
         (taskid (or
                  (cdr (assoc "JIRA_TASK_ID" properties))
                  (progn (mojira--set-task-id)
                         (cdr (assoc "JIRA_TASK_ID" (org-entry-properties))))))
         (clocksum (cdr (assoc "CLOCKSUM" properties))))
    (cond
     (taskid (mojira--log-work taskid clocksum))
     ((> (org-current-level) 1)
      (progn
        (org-up-element)
        (mojira-log-work-for-task-at-point)))
     (t (message "No task at point")))))

(defvar mojira-transitions
  '(("WEB" . (("To Do" . 11)
              ("In Progress" . 21)
              ("Done" . 31)
              ("PULL REQUEST" . 41)
              ("DEV" . 51)
              ("Cancelled" . 61)
              ("Obsolete" . 71)
              ("Ready for Homolog" . 81)))
    ("TC" . (("To Do" . 11)
             ("In Progress" . 21)
             ("Done" . 31)
             ("PULL REQUEST" . 41)
             ("DEV" . 51)
             ("STAGING" . 61)))
    ("PDPP" . (("To Do" . 11)
               ("In Progress" . 21)
               ("Done" . 31)
               ("PULL REQUEST" . 41)
               ("DEV" . 51)
               ("Cancelled" . 61)
               ("Obsolete" . 71)
               ("Ready for Homolog" . 81))))
  "Status Transition maps obtained from
GET :atlassian-v2/issue/:issue/transitions")

(defun mojira--get-board-abbrev (issue)
  (car (split-string issue "-")))

(defun mojira--get-transition (issue status)
  (cdr (assoc status
              (assoc (mojira--get-board-abbrev issue) mojira-transitions))))

(defun mojira--update-status (issue status)
  "Update jira issue status"
  (let ((transition-id (mojira--get-transition issue status)))
    (request
      (format "%s/issue/%s/transitions" mojira--api-v2 issue)
      :parser 'json-read
      :data (json-encode `(("transition" . (("id" . ,transition-id)))))
      :headers `(("Content-Type" . "application/json")
                 ("Authorization" . ,(concat "Basic " (mojira--get-base64-token))))
      :success
      (cl-function
       (lambda (&key data &allow-other-keys)
         (when data
           (message "%S" data)))))))

(defun mojira--move-to-status (status org-todo-status)
  (interactive)
  (org-back-to-heading t)
  (let* ((properties (org-entry-properties))
         (taskid (or
                  (cdr (assoc "JIRA_TASK_ID" properties))
                  (progn (mojira--set-task-id)
                         (cdr (assoc "JIRA_TASK_ID" (org-entry-properties))))))
         (todo (cdr (assoc "TODO" properties)))
         (clocksum (cdr (assoc "CLOCKSUM" properties))))
    (cond
     (taskid (progn
               (mojira--update-status taskid status)
               (org-todo org-todo-status)))
     ((> (org-current-level) 1)
      (progn
        (org-up-element)
        (mojira--move-to-status status)))
     (t (message "No task at point")))))

(defun mojira-move-to-done ()
  (interactive)
  (mojira--move-to-status "Done" "DONE"))

(defun mojira-move-to-pr ()
  (interactive)
  (mojira--move-to-status "PULL REQUEST" "FOLLOWUP"))

(defun mojira-move-to-in-progress ()
  (interactive)
  (mojira--move-to-status "In Progress" "WORKING"))

(defun mojira-move-to-todo ()
  (interactive)
  (mojira--move-to-status "To Do" "TODO"))

(defvar mojira-mappings
  '(("WEB" . (("customfield_10059" . "JIRA_STORY_POINTS")
              ("summary" . "SUMMARY")))
    ("TC" . ())
    ("PDPP" . ()))
  "Mappings from issue to org mode
GET :atlassian-v2/issue/:issue")

(defun mojira--get-reverse-mappings (project org-key)
  "Returns jira key from `project' and `org-key'"
  (let ((reverse-mappings (mapcar
                           (lambda (item) `(,(cdr item) . ,(car item)))
                           (cdr (assoc project mojira-mappings)))))
    (cdr (assoc org-key reverse-mappings))))

(defun mojira--get-mappings (issue)
  (cdr (assoc (mojira--get-board-abbrev issue) mojira-mappings)))

(defun mojira--get-mapping (issue field)
  (cdr (assoc field
              (assoc (mojira--get-board-abbrev issue) mojira-mappings))))

(defun mojira-get-issue-at-point ()
  "Get jira issue at point data"
  (interactive)
  (save-excursion
    (org-back-to-heading t)
    (org-columns)
    (org-columns-quit)
    (let* ((properties (org-entry-properties))
           (display-name (or (cdr (assoc "JIRA_USER" properties))
                             mojira--user))
           (taskid (or
                    (cdr (assoc "JIRA_TASK_ID" properties))
                    (progn (mojira--set-task-id)
                           (cdr (assoc "JIRA_TASK_ID" (org-entry-properties)))))))
      (cond
       (taskid (mojira--get-issue
                taskid
                (lambda (data)
                  (let* ((taskid (alist-get 'key data))
                         (mappings (mojira--get-mappings taskid)))
                    (mapc (lambda (mapping)
                            (let ((jira-name (car mapping))
                                  (org-name (cdr mapping)))
                              (org-set-property
                               org-name
                               (format "%S"
                                       (mojira--get-issue-field-property data jira-name)))))
                          mappings))
                  (message "Done"))))
       ((> (org-current-level) 1)
        (progn
          (org-up-element)
          (mojira-get-issue-at-point)))
       (t (message "No task at point"))))))

(defun mojira-create-issue-at-point ()
  "Create a jira issue and org node at point"
  (interactive)
  (save-excursion
    (org-back-to-heading t)
    (org-columns)
    (org-columns-quit)
    (let* ((properties (org-entry-properties))
           (item (cdr (assoc "ITEM" properties)))
           (storypoints (string-to-number (cdr (assoc "JIRA_STORY_POINTS" properties))))
           (issue-type (cdr (assoc "JIRA_ISSUE_TYPE" properties)))
           (projectid (cdr (assoc "JIRA_PROJECT_ID" properties)))
           (boardabbrev (cdr (assoc "JIRA_BOARD" properties)))
           (storypoints-key (mojira--get-reverse-mappings boardabbrev "JIRA_STORY_POINTS")))

      (lexical-let ((task-line item))
        (mojira--create-issue
         `((fields . ((summary . ,item)
                      (,storypoints-key . ,storypoints)
                      (issuetype . ((id . ,issue-type)))
                      (project . ((id . ,projectid)))))
           )
         (lambda (data)
           (let ((taskid (alist-get 'key data)))
             (org-set-property "JIRA_TASK_ID" taskid)
             (org-edit-headline (format "%s %s" taskid task-line)))
           )))
      )))

(defalias 'mojira-issue-create 'mojira-create-issue-at-point)
(defalias 'mojira-get 'mojira-get-issue-at-point)
(defalias 'mojira-worklog-get 'mojira-get-logged-for-task-at-point)
(defalias 'mojira-worklog-write 'mojira-log-work-for-task-at-point)
(defalias 'mojira-done 'mojira-move-to-done)
(defalias 'mojira-todo 'mojira-move-to-todo)
(defalias 'mojira-pr 'mojira-move-to-pr)
(defalias 'mojira-working 'mojira-move-to-in-progress)

(provide 'mojira)
